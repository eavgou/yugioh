import { async, ComponentFixture, TestBed , fakeAsync, tick } from '@angular/core/testing';

import { DebugElement, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { By } from '@angular/platform-browser';

import { CardListComponent } from './card-list.component';
import { CardItemComponent } from './card-item/card-item.component';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';
import { CardService } from '../services/card.service';
import { HttpClientModule } from '@angular/common/http';
import { CARDS } from '../mock-deck';
import { CardDetailsComponent } from '../card-details/card-details.component';
import { TestCardService } from '../test/test-card.service';

let component: CardListComponent;
let fixture: ComponentFixture<CardListComponent>;
// let page: Page;

describe('CardListComponent', () => {


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardItemComponent, CardListComponent, CardDetailsComponent ],
      imports: [RouterTestingModule, HttpClientModule],
      providers: [{ provide: CardService, useClass: TestCardService }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents().then(createComponent);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

 it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('should display cards', () => {
    const cardItems = fixture.nativeElement.querySelectorAll('app-card-item');
    expect(Array.from(cardItems).length).toBeGreaterThan(0);
  });



/** Create the component */
function createComponent() {
  fixture = TestBed.createComponent(CardListComponent);
  component = fixture.componentInstance;

  fixture.detectChanges();

  return fixture.whenStable().then(() => {
    fixture.detectChanges();
  });
}

});
