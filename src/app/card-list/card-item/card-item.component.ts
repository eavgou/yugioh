import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Card } from '../../card.model';
import { CardService } from '../../services/card.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-card-item',
  templateUrl: './card-item.component.html',
  styleUrls: ['./card-item.component.scss']
})
export class CardItemComponent implements OnInit {
  card: Card;
  selectedCard: Card;
  @Input() name: string;

  constructor(private router: Router, private cardService: CardService) { }

  ngOnInit() {
    this.cardService.getCardDetails(this.name).subscribe(
      data => this.card = data,
      error => {
        console.error(error.message);
       } );

  }

}
