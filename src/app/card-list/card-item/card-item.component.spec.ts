import { async, inject, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { DebugElement } from '@angular/core';

import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { Card } from '../../card.model';
import { CardService } from '../../services/card.service';
import { Observable } from 'rxjs/Observable';
import { CardItemComponent } from './card-item.component';
import { CARDS } from '../../mock-deck';
import { TestCardService } from '../../test/test-card.service';

let component: CardItemComponent;
let fixture: ComponentFixture<CardItemComponent>;

describe('CardItemComponent', () => {

  const routerSpy = jasmine.createSpyObj('Router', ['navigate']);


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardItemComponent ],
      imports: [RouterTestingModule, HttpClientModule],
      providers: [{ provide: CardService, useClass: TestCardService },
        { provide: Router, useValue: routerSpy}],
    })
    .compileComponents()
    .then(createComponent);
  }));


  it('should create', () => {
    expect(component).toBeTruthy();
  });

/* it('should navigate to selected card detail on click', fakeAsync(() => {
    const expectedCard = CARDS[1];

    const link = fixture.nativeElement.querySelectorAll('a.list-group-item');
    link.dispatchEvent(newEvent('click'));
    tick();

    // should have navigated
    // const routerSpy = fixture.debugElement.injector.get(Router);
    const navSpy = routerSpy.navigate as jasmine.Spy;

    expect(navSpy.calls.any()).toBe(true, 'navigate called');

    // expect link array with the route path and card name
    // first argument to router.navigate is link array
    const navArgs = navSpy.calls.first().args[0];
    expect(navArgs[0]).toContain('cards', 'nav to cards detail URL');
    expect(navArgs[1]).toBe(expectedCard.name, 'expected card.name');

  }));*/

});

/** Create the component */
function createComponent() {
  fixture = TestBed.createComponent(CardItemComponent);
  component = fixture.componentInstance;

  // component.card = { id: 12, name: 'Charge of the Light Brigade' , text: '', card_type: '', type: '', family: '' };
//  component.name = 'Charge of the Light Brigade';

  fixture.detectChanges();

  return fixture.whenStable().then(() => {
    fixture.detectChanges();
  });
}



function newEvent(eventName: string, bubbles = false, cancelable = false) {
  const evt = document.createEvent('CustomEvent');  // MUST be 'CustomEvent'
  evt.initCustomEvent(eventName, bubbles, cancelable, null);
  return evt;
}


