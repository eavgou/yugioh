import { Component, OnInit , Input } from '@angular/core';

import { CARDS } from '../mock-deck';
import { Card } from '../card.model';

@Component({
  selector: 'app-card-list',
  templateUrl: './card-list.component.html',
  styleUrls: ['./card-list.component.scss']
})
export class CardListComponent implements OnInit {
  cards: Card[];

  constructor() { }

  ngOnInit() {
    this.cards = CARDS;
  }

}
