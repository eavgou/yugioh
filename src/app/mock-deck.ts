import { Card } from './card.model';

export const CARDS: Card[] = [
  { id: 11, name: 'Burial from a Diﬀerent Dimension', text: '', card_type: '', type: '', family: '' },
  { id: 12, name: 'Charge of the Light Brigade', text: '', card_type: '', type: '', family: '' },
  { id: 13, name: 'Infernoid Antra' , text: '', card_type: '', type: '', family: '' },
  { id: 14, name: 'Infernoid Attondel' , text: '', card_type: '', type: '', family: '' },
  { id: 15, name: 'Infernoid Decatron' , text: '' , card_type: '', type: '', family: ''},
  { id: 16, name: 'Infernoid Devyaty' , text: '' , card_type: '', type: '', family: ''},
  { id: 17, name: 'Infernoid Harmadik' , text: '' , card_type: '', type: '', family: ''},
  { id: 18, name: 'Infernoid Onuncu' , text: '' , card_type: '', type: '', family: ''},
  { id: 19, name: 'Infernoid Patrulea' , text: '', card_type: '', type: '', family: '' },
  { id: 20, name: 'Infernoid Pirmais', text: '' , card_type: '', type: '', family: '' },
  { id: 21, name: 'Infernoid Seitsemas', text: '' , card_type: '', type: '', family: '' },
  { id: 22, name: 'Lyla, Lightsworn Sorceress' , text: '' , card_type: '', type: '', family: ''},
  { id: 23, name: 'Monster Gate', text: '', card_type: '', type: '', family: ''  },
  { id: 24, name: 'One for One', text: '' , card_type: '', type: '', family: '' },
  { id: 25, name: 'Raiden, Hand of the Lightsworn', text: '' , card_type: '', type: '', family: '' },
  { id: 26, name: 'Reasoning', text: '' , card_type: '', type: '', family: '' },
  { id: 27, name: 'Time-Space Trap Hole', text: '' , card_type: '', type: '', family: '' },
  { id: 28, name: 'Torrential Tribute' , text: '' , card_type: '', type: '', family: ''},
  { id: 29, name: 'Upstart Goblin', text: '' , card_type: '', type: '', family: '' },
  { id: 30, name: 'Void Seer', text: '' , card_type: '', type: '', family: ''}
];
