export class Card {
    public id: number;
    public name: string;
   public text: string;
    public card_type: string;
    public type: string;
    public family: string;

    constructor(id: number, name: string, text?: string, card_type?: string, type?: string,
    family?: string) {
        this.id = id;
        this.name = name;
        this.text = text;
        this.card_type = card_type;
        this.type = type;
        this.family = family;
    }
  }

