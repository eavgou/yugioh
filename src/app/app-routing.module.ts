import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

import { CardDetailsComponent } from './card-details/card-details.component';
import { CardListComponent } from './card-list/card-list.component';
import { CARDS } from './mock-deck';

const appRoutes: Routes = [
  { path: 'cards/:id', component: CardDetailsComponent},
  { path: '', redirectTo: 'cards/' + CARDS[1].name, pathMatch: 'full' },
  
  { path: '**', redirectTo: 'cards/' + CARDS[1].name, pathMatch: 'full' 
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {preloadingStrategy: PreloadAllModules})
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
