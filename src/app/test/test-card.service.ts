import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/observable';
import { CARDS } from '../mock-deck';
import { Card } from '../card.model';

import { map } from 'rxjs/operators';
import { CardService } from '../services/card.service';
import { defer } from 'rxjs/observable/defer';

/** Create async observable that emits-once and completes
 *  after a JS engine turn */
function asyncData<T>(data: T) {
  return defer(() => Promise.resolve(data));
}

@Injectable()
/**
 * FakeCardService pretends to make real http requests.
*/
export class TestCardService extends CardService {

  constructor() {
    super(null);
  }

  cards = CARDS;
  lastResult: Observable<Card>; // result from last method call


  getCardDetails(name: string): Observable<Card> {
    const card = this.cards.find(c => c.name === name);
    return this.lastResult = asyncData(card);
  }

}

