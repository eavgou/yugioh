import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardDetailsComponent } from './card-details.component';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';

import { CardService } from '../services/card.service';
import { Observable } from 'rxjs/Observable';

import { CARDS } from '../mock-deck';
import { TestCardService } from '../test/test-card.service';

import 'rxjs/add/observable/of';

describe('CardDetailsComponent', () => {
  let component: CardDetailsComponent;
  let fixture: ComponentFixture<CardDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardDetailsComponent ],
      imports: [RouterTestingModule, HttpClientModule],

      providers: [{ provide: CardService, useClass: TestCardService }, {provide: ActivatedRoute,
        useValue: {
          params: Observable.of({id: 'Charge of the Light Brigade'})
        }}]
    })
    .compileComponents().then(createComponent);
  }));


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display card\'s name', async(() => {
    const expectedCard = CARDS[1];

    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h3').textContent).toBe(expectedCard.name);
  }));
  

   /////////// Helpers /////

function createComponent() {
  fixture = TestBed.createComponent(CardDetailsComponent);
  component = fixture.componentInstance;
  component.card = { id: 12, name: 'Charge of the Light Brigade' , text: '', card_type: '', type: '', family: '' };

  fixture.detectChanges();
  return fixture.whenStable().then(() => {
    fixture.detectChanges();
  });
}
});
