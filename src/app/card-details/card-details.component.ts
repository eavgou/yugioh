import { Component, OnInit, Input } from '@angular/core';
import { Card } from '../card.model';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { CardService } from '../services/card.service';

@Component({
  selector: 'app-card-details',
  templateUrl: './card-details.component.html',
  styleUrls: ['./card-details.component.scss']
})
export class CardDetailsComponent implements OnInit {
  card: Card;
  name: string;

  constructor(private route: ActivatedRoute,
    private router: Router, private cardService: CardService) {
  }

  ngOnInit() {
    // Read pamameter containing card's name
    this.route.params
      .subscribe(
        (params: Params) => {
          this.getCard(params['id']);
      }
    );
  }
  private getCard(name: string): void {
      this.cardService.getCardDetails(name).subscribe(
        data => this.card = data,
        error => {
          console.error(error.message);
        }
     );
  }
}
