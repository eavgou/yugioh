import { Http, Response } from '@angular/http';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Card } from '../card.model';

import { catchError, map, tap } from 'rxjs/operators';
import 'rxjs/add/operator/publishReplay';


@Injectable()
export class CardService {
    apiRoot = 'http://52.57.88.137/api/card_data';

constructor(
    private http: HttpClient) { }


    // Uses http.get() to load data from a single API endpoint
    getCardDetails(cardName: string) {
        return this.http.get<Card>(this.apiRoot + '/' + cardName).pipe(
        map(data => data['data']),
        catchError(this.handleError<Card>(`getCard name=${cardName}`))
        ).publishReplay(1) // this tells Rx to cache the latest emitted value
        .refCount(); // and this tells Rx to keep the Observable alive as long as there are any Subscribers
    }



  /**
   * Returns a function that handles Http operation failures.
   * This error handler lets the app continue to run as if no error occurred.
   * @param operation - name of the operation that failed
   */
  private handleError<T> (operation = 'operation') {
    return (error: HttpErrorResponse): Observable<T> => {

    //  console.error(error); // log to console instead

      const message = (error.error instanceof ErrorEvent) ?
        error.error.message :
       `server returned code ${error.status} with body "${error.message}"`;

      throw new Error(`${operation} failed: ${message}`);
    };

  }

}
