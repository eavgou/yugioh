import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { CardListComponent } from './card-list/card-list.component';
import { RouterTestingModule } from '@angular/router/testing';
import { CardDetailsComponent } from './card-details/card-details.component';
import { CardItemComponent } from './card-list/card-item/card-item.component';
import { CardService } from './services/card.service';
import { HttpClientModule } from '@angular/common/http';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        HeaderComponent,
        CardListComponent,
        AppComponent,
        CardDetailsComponent,
        CardItemComponent
      ],
      imports: [RouterTestingModule, HttpClientModule],
      providers: [CardService]
    }).compileComponents();
  }));
 it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it('should render list title in a h2 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h2').textContent).toContain('Cards');
  }));


});
